

#include "headers.h"
#include "image_streaming.h"

#include "udp_socket.h"
#include "utility.h"
#include "packet_aggregator.h"


void startThreads(int argc, char** argv);

/*void testPacketAggregator();
void creatDir(string file_path);
string GetCurrentWorkingDir();*/
int main( int argc, char** argv )
{
  startThreads(argc, argv);

  return 0;
}


void startThreads(int argc, char** argv) {

  ImageStreaming* dataPool = new ImageStreaming(argc, argv);

  int num = 2;
  pthread_t threads[num];

  pthread_create (&(threads[0]), NULL, &ImageStreaming::GstreamerReceiver, dataPool);
  //pthread_create (&(threads[1]), NULL, &ImageStreaming::ControlPanel, dataPool);
  pthread_create (&(threads[1]), NULL, &ImageStreaming::VideoFrameProcesser, dataPool);
  pthread_create (&(threads[2]), NULL, &ImageStreaming::UDPReceiverForCar, dataPool);

  for(int i = 0; i < num; ++i) {
    pthread_join(threads[i], NULL);
  }

  delete dataPool;
}





//////////////////////////backup////////////////////////////


//get the local file path
/*
std::string GetCurrentWorkingDir( void ) {
  char buff[FILENAME_MAX];
  GetCurrentDir( buff, FILENAME_MAX );
  std::string current_working_dir(buff);
  return current_working_dir;
}

//creat a new directory
void creatDir(string file_path){
	std::string dir = "mkdir -p " + GetCurrentWorkingDir() + file_path;
	const int dir_err = system(dir.c_str());
	if (-1 == dir_err)
	{
	    printf("Error creating directory!n");
	    exit(1);
	}
}


void testPacketAggregator() {
  PacketAggregator packetAggregator;
  int frameLen = 38000;
  string payload(frameLen, 'x');

  FrameData frameData;
  frameData.compressedDataSize = frameLen;
  frameData.frameSendTime = 123;
  frameData.transmitSequence = 1;
  frameData.compressedDataSize = payload.size();


  vector<PacketAndData> packets = packetAggregator.deaggregatePackets(frameData, payload, 0.04);

  for (int i = 0; i < packets.size(); ++i) {
    PacketAndData cur = packets[i];
    packetAggregator.insertPacket(cur.first, cur.second);
  }
  for (int i = 0; i < packetAggregator.videoFrames.size(); ++i) {
    FrameAndData frameAndData = packetAggregator.videoFrames[i];
  }
}

*/







