################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/data_model.cpp \
../src/fec.cpp \
../src/image_streaming.cpp \
../src/main.cpp \
../src/packet_aggregator.cpp \
../src/udp_socket.cpp \
../src/utility.cpp 

OBJS += \
./src/data_model.o \
./src/fec.o \
./src/image_streaming.o \
./src/main.o \
./src/packet_aggregator.o \
./src/udp_socket.o \
./src/utility.o 

CPP_DEPS += \
./src/data_model.d \
./src/fec.d \
./src/image_streaming.d \
./src/main.d \
./src/packet_aggregator.d \
./src/udp_socket.d \
./src/utility.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/lib/x86_64-linux-gnu/gstreamer-1.0/include -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/glib-2.0 -I/usr/include/gstreamer-1.0 -O2 -g -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


